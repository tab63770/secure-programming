package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class loginController implements Initializable {
	public loginModel loginModel = new loginModel();
	@FXML
	public Label isConnected;
	@FXML
	private TextField user1;
	@FXML
	private TextField pass1;
	@FXML
	private AnchorPane loginPane;
	@FXML
	private Button loginBtn;
	@FXML
	private Hyperlink registerLink;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}
	
	
	

	public void login1(ActionEvent event) throws IOException {
		Stage stage;
		Parent root;
		try {
			if (event.getSource() == registerLink) {
				stage = (Stage) registerLink.getScene().getWindow();
				root = FXMLLoader.load(getClass().getResource("/application/Register.fxml"));
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
			} else {
				if (loginModel.isLogin(user1.getText(), pass1.getText())) // login successful
				{
					// Now the Welcome page is loaded
					String use = loginModel.getUserName(user1.getText());
					FXMLLoader loader = new FXMLLoader();
					AnchorPane pane = loader.load(getClass().getResource("/application/Welcome.fxml").openStream());
					WelcomeController welcomeController = (WelcomeController) loader.getController();
					welcomeController.getUser(use);
					loginPane.getChildren().setAll(pane);
				} else {
					// As login is failed , the Error Alert window mentioning
					// the same
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error!!");
					alert.setHeaderText("Wrong user name or password");
					alert.setContentText("Please try login with proper credentials");
					alert.showAndWait();
					isConnected.setText("Please try login again");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			isConnected.setText("Incorrect username or password");
			e.printStackTrace();
		}
	}
}
