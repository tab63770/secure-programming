package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class WelcomeController implements Initializable {
	public loginModel loginModel2 = new loginModel();
	@FXML
	private Label txtUserName1;
	@FXML
	private Button logOutBtn;
	@FXML
	private AnchorPane welcomePane;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}

	/*
	 * @param use - username from the login User Name textBox
	 * 
	 * @return void This method set the Label text to UserName which appears on
	 * Welcome Page
	 */
	public void getUser(String use) {
		txtUserName1.setText(use);
	}

	/*
	 * When logout button is clicked on the Welcome page , 'logout' method is
	 * triggered
	 * 
	 */
	public void logout(ActionEvent event) {
		Stage stage1;
		Parent root1;
		if (event.getSource() == logOutBtn) // if button clicked is logout
											// button
		{
			try {
				loginModel2.logout1();
				// load login page
				stage1 = (Stage) logOutBtn.getScene().getWindow();
				root1 = FXMLLoader.load(getClass().getResource("/application/login.fxml"));
				Scene scene1 = new Scene(root1);
				stage1.setScene(scene1);
				stage1.show();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error!!");
			alert.setHeaderText("SomeThing Went Wrong");
			alert.showAndWait();
		}
	}
}
