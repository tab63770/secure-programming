package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

public class RegisterController implements Initializable {
	public loginModel loginModel1 = new loginModel();
	@FXML
	private AnchorPane registerPane;
	@FXML
	private Label txtStatus;
	@FXML
	private Button registerBtn2;
	@FXML
	private TextField txtUserId;
	@FXML
	private TextField txtFirstName;
	@FXML
	private TextField txtLastName;
	@FXML
	private TextField txtEmail;
	@FXML
	private PasswordField txtPassword1;
	@FXML
	private PasswordField txtPassword2;
	private String flag;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}
	
	
	   public boolean usernamecheck() {
		      // String to be scanned to find the pattern.
		      String pat_user = "^[a-zA-Z0-9]{3,10}$";
		      // Create a Pattern object
		      Pattern r = Pattern.compile(pat_user);
		      // Now create matcher object.
		      Matcher m = r.matcher(txtUserId.getText());
		      if (m.find( )) {
		        return true;
		      }else {
		         flag= flag+" INVALID Username ";
		         return false;
		      }
		   
	   }
	   
	   public boolean Firstcheck() {
		      // String to be scanned to find the pattern.
		      String pat_First = "^[a-zA-Z]{2,12}$";
		      // Create a Pattern object
		      Pattern r = Pattern.compile(pat_First);
		      // Now create matcher object.
		      Matcher m = r.matcher(txtFirstName.getText());
		      if (m.find( )) {
		        return true;
		      }else {
		         flag= flag+" INVALID First Name ";
		         return false;
		      }
		   
	   }	   
	   
	   public boolean Lastcheck() {
		      // String to be scanned to find the pattern.
		   String pat_Last = "^[a-zA-Z_]{2,12}$";
		      // Create a Pattern object
		      Pattern r = Pattern.compile(pat_Last);
		      // Now create matcher object.
		      Matcher m = r.matcher(txtLastName.getText());
		      if (m.find( )) {
		        return true;
		      }else {
		         flag= flag+" INVALID Last Name ";
		         return false;
		      }
		   
	   }
	   
	   public boolean mailcheck() {
		      // String to be scanned to find the pattern.
		   String pat_email = "^(?=.{1,40}$)[a-zA-z_.0-9]+@[a-zA-z0-9]+\\.[a-z]{1,3}$";
		      // Create a Pattern object
		      Pattern r = Pattern.compile(pat_email);
		      // Now create matcher object.
		      Matcher m = r.matcher(txtEmail.getText());
		      if (m.find( )) {
		        return true;
		      }else {
		         flag= flag+" INVALID email ";
		         return false;
		      }
		   
	   }	   
	   
	   
	   public boolean passcheck1() {
		      // String to be scanned to find the pattern.
		   String pat_pass = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
		      // Create a Pattern object
		      Pattern r = Pattern.compile(pat_pass);
		      // Now create matcher object.
		      Matcher m = r.matcher(txtPassword1.getText());
		      if (m.find( )) {
		        return true;
		      }else {
		         flag= flag+" INVALID password one ";
		         return false;
		      }
		   
	   }	   

	   public boolean passcheck2() {
		      // String to be scanned to find the pattern.
		   String pat_pass = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
		      // Create a Pattern object
		      Pattern r = Pattern.compile(pat_pass);
		      // Now create matcher object.
		      Matcher m = r.matcher(txtPassword2.getText());
		      if (m.find( )) {
		        return true;
		      }else {
		         flag= flag+" INVALID password two ";
		         return false;
		      }
		   
	   }
	   
	   public boolean samepasscheck() {
		   String pass1=txtPassword1.getText();
		   String pass2=txtPassword2.getText();
		   
		      if (pass1.equals(pass2)) {
		        return true;
		      }else {
		         flag= flag+" INVALID Passwords do not match ";
		         return false;
		      }
	   }
	   
	   
	 
	// 'navigate2' method is invoked when register or login button is clicked
	public void navigate2(ActionEvent event) throws IOException {
		Stage stage1;
		Parent root1;
		flag = "";
		try {
			if (event.getSource() == registerBtn2) // if register button is clicked
			{
				//test inputs
				
				if (usernamecheck() && Firstcheck() && Lastcheck() && mailcheck() && passcheck1() && passcheck2() && samepasscheck() && loginModel1.insertUser(txtUserId.getText(), txtFirstName.getText(), txtLastName.getText(),
						txtEmail.getText(), txtPassword1.getText())) // if registration successful
				{
					// all the entries in the textboxes are cleared
					txtUserId.setText("");
					txtFirstName.setText("");
					txtLastName.setText("");
					txtEmail.setText("");
					txtPassword1.setText("");
					txtPassword2.setText("");

					// Registration success info alert is displayed
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setHeaderText("Registration Successful");
					alert.setContentText("Please Login");
					alert.showAndWait();
					// and then return to login screen
					stage1 = (Stage) registerBtn2.getScene().getWindow();
					root1 = FXMLLoader.load(getClass().getResource("/application/login.fxml"));
					Scene scene1 = new Scene(root1);
					stage1.setScene(scene1);
					stage1.show();
				} else {// enter of registration is unsuccessful and display
						// error alert
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error!!");
					alert.setHeaderText("Registration Unsuccessful Please try again");
					alert.setContentText(flag);
					alert.showAndWait();
					txtStatus.setText("Please try registering again");
				}
			}

			else // if login hyperlink is clicked , login page is loaded
			{
				FXMLLoader loader = new FXMLLoader();
				AnchorPane pane3 = loader.load(getClass().getResource("/application/login.fxml").openStream());
				registerPane.getChildren().setAll(pane3);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
