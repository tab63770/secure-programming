package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/*
 * This class contains all the logic for loginController,RegisterController,WelcomeController(login,Register,Welcome Pages)
 * which acts as a server
 *
 */
public class loginModel {
	/*
	 * @param user -- data entered in the username textbox of login page
	 * 
	 * @param pass -- password entered in the password textbox of login page
	 * 
	 * @return true or false If login is successful returns true else false
	 */
	public Boolean isLogin(String user, String pass) {
		BufferedReader br = null;
		try {
			String line;

			br = new BufferedReader(new FileReader("login_DB.txt"));
			while ((line = br.readLine()) != null) {
				String[] dataRead = line.split("\\-");
					{
					final String k = "passhashtohashpass";
					String user1 = AES.decrypt(dataRead[0], k);
					String first = AES.decrypt(dataRead[1], k);
					String last = AES.decrypt(dataRead[2], k);
					String e = AES.decrypt(dataRead[3], k);
					byte[] salts = Passwords.getBytes(dataRead[6]);
					byte[] pass1 = Passwords.getBytes(dataRead[4]);
					if (((user1.equals(user) || (e.equals(user))) && (Passwords.isExpectedPassword(pass.toCharArray(), salts, pass1)))) {
						return true;// status =true;
					}
				}
			}
			return false;
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getStackTrace();
			return false;

		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

	}
	
public static int randInt(int min, int max) {
	
	Random rand = null;
	
	int randomNum = rand.nextInt((max - min) + 1) + min;
	
	return randomNum;	
}

 

	/*
	 * @param user -- data entered in the username textbox of login page
	 * 
	 * @return string 'getUserName' method returns a string --> Name of the user
	 * Additional info: This method is invoked after the login page if login
	 * successful
	 */
	public String getUserName(String user) {

		BufferedReader br = null;
		try {
			String line;

			br = new BufferedReader(new FileReader("login_DB.txt"));
			while ((line = br.readLine()) != null) {
				String[] dataRead = line.split("\\-");
{
					String acctype= "";
					final String k = "passhashtohashpass";
					String user1 = AES.decrypt(dataRead[0], k);
					String first = AES.decrypt(dataRead[1], k);
					String last = AES.decrypt(dataRead[2], k);
					String e = AES.decrypt(dataRead[3], k);
					String usertype = AES.decrypt(dataRead[5], k);
					if (usertype.equals("3")) {
						acctype = "Regular Account";
					}
					if (usertype.equals("2")) {
						acctype = "Admin Account";
					}
					if (usertype.equals("1")) {
						acctype = "Super User Account";
					}
				if ((user1.equals(user) || (e.equals(user)))) {
					return (first + " " + last + " your account is set to " + acctype + ".");// status =true;
				}
				}
			}
			return "Alien";
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getStackTrace();
			return "Alien";

		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

	}

	/*
	 * @param userId -- userId entered in the 'User ID' textbox of registration
	 * page
	 * 
	 * @param firstName -- userId entered in the 'First Name' textbox of
	 * registration page
	 * 
	 * @param lastName -- userId entered in the 'Last Name' textbox of
	 * registration page
	 * 
	 * @param email -- userId entered in the 'Email' textbox of registration
	 * page
	 * 
	 * @param password -- password entered in the 'Password' textbox of login
	 * page
	 * 
	 * @return true or false 'insertUser' method tries to write the user
	 * registration data to text file, if successful returns true else returns
	 * false AdditionalInfo: 'insertUser' method is invoked after user click on
	 * 'Register' button in 'Registration' page.
	 */
	public Boolean insertUser(String userId, String firstName, String lastName, String email, String password) {
		

int randomInt = ThreadLocalRandom.current().nextInt(1,4);

int num = 0;
num = randomInt; 
		
		String usertype= num + "";
		final String k = "passhashtohashpass";
		String user = AES.encrypt(userId, k);
		String first = AES.encrypt(firstName, k);
		String last = AES.encrypt(lastName, k);
		String e_mail = AES.encrypt(email, k);
		String user_type = AES.encrypt(usertype, k);
		byte[] salts = Passwords.getNextSalt();
		String pass = Passwords.getString(Passwords.hash(password.toCharArray(), salts));
		BufferedWriter write = null;
		String record = user + "-" + first + "-" + last + "-" + e_mail + "-" + pass + "-" + user_type + "-" + Passwords.getString(salts);
		try {
			write = new BufferedWriter(new FileWriter("login_DB.txt", true));
			write.write(record);
			write.newLine();
			write.flush();
			return true;
		} catch (IOException e1) {
			e1.printStackTrace();
			// TODO Auto-generated catch block
			return false;

		} finally {
			try {
				write.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * When user click on the logout button in the Welcome Page , method
	 * 'logout1' is invoked and logout the user. Additional info: The view was
	 * configured to load login page when user click on logout button.
	 */
	public void logout1() {

	}

}